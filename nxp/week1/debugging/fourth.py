#!/usr/bin/python

def double(a):
    a = a * 2
    return a * a

def square(a):
    double(a)
    return a * a

def my_func():
    a = 10
    square(a)
    return a

my_func()