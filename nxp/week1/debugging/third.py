#!/usr/bin/python
import pdb

def first():
	second()
	return "hey i am in first."

def second():
	third()
	return "hey i am in second"

def third():
	fourth()
	return "hey i am in third"

def fourth():
	fifth()
	return "hey i am in fourth"

def fifth():
	return "hey i am in fifth"

pdb.set_trace()
first()