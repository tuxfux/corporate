#!/usr/bin/python
#import pdb

version=3.0

def my_add(a,b):
	''' This is for addition of numbers and string '''
	print "value of {}".format(a)
	print "value of {}".format(b)
	return a + b

def my_sub(a,b):
	''' This is for substratction of two numbers '''
	return a - b

def my_div(a,b):
	''' this is for division of two numbers '''
	return a/b

def my_multi(a,b):
	''' This is for multiplication of two numbers '''
	return a * b

# Main
if __name__ == '__main__':
	print "This is for learning debugging."
	print "we are going to use pdb for debugging."
#	pdb.set_trace()
	print "Addition of two numbers is {}".format(my_add(23,24))
	print "Substraction of two numbers is {}".format(my_sub(24,21))
	print "division of two numbers is {}".format(my_div(24,2))
	print "multiplication of two numbers is {}".format(my_multi(8,2))
