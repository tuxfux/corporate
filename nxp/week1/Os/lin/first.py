#!/usr/bin/python

def my_lin1_first():
  ''' This is my first lin1 function '''
  return "This is my first lin1 function"

def my_lin1_second():
  ''' this is my second lin1 function '''
  return "this is my second lin1 function"

def my_lin1_third():
  ''' This is my third lin1 function '''
  return "This is my third lin1 function"

def my_lin1_fourth():
  ''' this is my fourth lin1 function '''
  return "This is my fourth lin1 function"
