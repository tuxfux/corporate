#!/usr/bin/python

def my_sol3_first():
  ''' This is my first sol3 function '''
  return "This is my first sol3 function"

def my_sol3_second():
  ''' this is my second sol3 function '''
  return "this is my second sol3 function"

def my_sol3_third():
  ''' This is my third sol3 function '''
  return "This is my third sol3 function"

def my_sol3_fourth():
  ''' this is my fourth sol3 function '''
  return "This is my fourth sol3 function"
