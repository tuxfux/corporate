#!/usr/bin/python

def my_sol1_first():
  ''' This is my first sol1 function '''
  return "This is my first sol1 function"

def my_sol1_second():
  ''' this is my second sol1 function '''
  return "this is my second sol1 function"

def my_sol1_third():
  ''' This is my third sol1 function '''
  return "This is my third sol1 function"

def my_sol1_fourth():
  ''' this is my fourth sol1 function '''
  return "This is my fourth sol1 function"
