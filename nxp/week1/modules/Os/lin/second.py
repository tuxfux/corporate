#!/usr/bin/python

def my_lin2_first():
  ''' This is my first lin2 function '''
  return "This is my first lin2 function"

def my_lin2_second():
  ''' this is my second lin2 function '''
  return "this is my second lin2 function"

def my_lin2_third():
  ''' This is my third lin2 function '''
  return "This is my third lin2 function"

def my_lin2_fourth():
  ''' this is my fourth lin2 function '''
  return "This is my fourth lin2 function"
