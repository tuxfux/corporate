#!/usr/bin/python

def my_lin3_first():
  ''' This is my first lin3 function '''
  return "This is my first lin3 function"

def my_lin3_second():
  ''' this is my second lin3 function '''
  return "this is my second lin3 function"

def my_lin3_third():
  ''' This is my third lin3 function '''
  return "This is my third lin3 function"

def my_lin3_fourth():
  ''' this is my fourth lin3 function '''
  return "This is my fourth lin3 function"
