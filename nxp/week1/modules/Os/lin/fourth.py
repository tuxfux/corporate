#!/usr/bin/python

def my_lin4_first():
  ''' This is my first lin4 function '''
  return "This is my first lin4 function"

def my_lin4_second():
  ''' this is my second lin4 function '''
  return "this is my second lin4 function"

def my_lin4_third():
  ''' This is my third lin4 function '''
  return "This is my third lin4 function"

def my_lin4_fourth():
  ''' this is my fourth lin4 function '''
  return "This is my fourth lin4 function"
