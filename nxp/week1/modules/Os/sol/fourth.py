#!/usr/bin/python

def my_sol4_first():
  ''' This is my first sol4 function '''
  return "This is my first sol4 function"

def my_sol4_second():
  ''' this is my second sol4 function '''
  return "this is my second sol4 function"

def my_sol4_third():
  ''' This is my third sol4 function '''
  return "This is my third sol4 function"

def my_sol4_fourth():
  ''' this is my fourth sol4 function '''
  return "This is my fourth sol4 function"
