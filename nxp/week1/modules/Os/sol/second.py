#!/usr/bin/python

def my_sol2_first():
  ''' This is my first sol2 function '''
  return "This is my first sol2 function"

def my_sol2_second():
  ''' this is my second sol2 function '''
  return "this is my second sol2 function"

def my_sol2_third():
  ''' This is my third sol2 function '''
  return "This is my third sol2 function"

def my_sol2_fourth():
  ''' this is my fourth sol2 function '''
  return "This is my fourth sol2 function"
