#!/usr/bin/python
# basicConfig?
# Formatter?
# man date
# https://docs.python.org/2/library/subprocess.html
# crontab
# scheduler


import logging
from subprocess import Popen,PIPE

# l.basicConfig(filename='my_disk.txt',filemode='a',level=l.DEBUG,
# 			format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',
# 			datefmt='%c')

# Loggers expose the interface that application code directly uses.
# ex: root
# Handlers send the log records (created by loggers) to the appropriate destination.
# ex: filename='my_disk.txt',filemode='a'
# Filters provide a finer grained facility for determining which log records to output.
# ex: level=l.DEBUG
# Formatters specify the layout of log records in the final output.
# format: format='%(asctime)s - %(levelname)s - %(name)s - %(message)s'

# create logger
logger = logging.getLogger('Disk Monitor') # logger name
logger.setLevel(logging.DEBUG)               # filter at a logger level.

# create console handler and set level to debug
ch = logging.FileHandler('my_disk.txt')  # StreamHandler
ch.setLevel(logging.DEBUG)    # filter at a handler level

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter) # handler vs formatter

# add ch to logger
logger.addHandler(ch)      # logger vs handler


#main
# disk_size = $(df -h /|tail -n 1|awk '{print $5}'|sed -e 's#%##g')

#disk_size = int(raw_input("please enter your disk_size:"))

p1 = Popen(['df','-h','/'],stdout=PIPE)
p2 = Popen(['tail','-n','1'],stdin=p1.stdout,stdout=PIPE)
output = p2.communicate()
disk_size = int(output[0].split()[-2].split('%')[0])

if disk_size < 50:
	logger.info("Your disk looks health at - {}".format(disk_size))
elif disk_size < 75:
	logger.warning("Your disk is puking warning - {}".format(disk_size))
elif disk_size < 80:
	logger.error("Urgent !! your disk is getting errors - {}".format(disk_size))
elif disk_size < 99:
	logger.critical("your application is dead - {}".format(disk_size))


'''
challenges
* we cannot change the logger name.
* handlers - filehander,streamhandler
* this is for small teams or applications.
'''