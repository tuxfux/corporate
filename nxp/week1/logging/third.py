#!/usr/bin/python
# basicConfig?
# Formatter?
# man date
# https://docs.python.org/2/library/subprocess.html
# crontab
# scheduler


import logging as l
from subprocess import Popen,PIPE

l.basicConfig(filename='my_disk.txt',filemode='a',level=l.DEBUG,
			format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',
			datefmt='%c')

#main
# disk_size = $(df -h /|tail -n 1|awk '{print $5}'|sed -e 's#%##g')

#disk_size = int(raw_input("please enter your disk_size:"))

p1 = Popen(['df','-h','/'],stdout=PIPE)
p2 = Popen(['tail','-n','1'],stdin=p1.stdout,stdout=PIPE)
output = p2.communicate()
disk_size = int(output[0].split()[-2].split('%')[0])

if disk_size < 50:
	l.info("Your disk looks health at - {}".format(disk_size))
elif disk_size < 75:
	l.warning("Your disk is puking warning - {}".format(disk_size))
elif disk_size < 80:
	l.error("Urgent !! your disk is getting errors - {}".format(disk_size))
elif disk_size < 99:
	l.critical("your application is dead - {}".format(disk_size))


'''
challenges
* we cannot change the logger name.
* handlers - filehander,streamhandler
* this is for small teams or applications.
'''