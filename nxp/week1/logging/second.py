#!/usr/bin/python
# basicConfig?
# Formatter?
# man date


import logging as l

l.basicConfig(filename='my_logs.txt',filemode='a',level=l.DEBUG,
			format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',
			datefmt='%c')

l.debug("this is a debug message")
l.info("this is an informational message")
l.warning("this is a warning message")
l.error("this is an error message")
l.critical("this is an critical message")