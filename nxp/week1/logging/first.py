#!/usr/bin/python
'''
The default level is WARNING, which means that only events 
of this level and above will be tracked,
unless the logging package is configured to do otherwise.
'''

import logging as l

l.debug("this is a debug message")
l.info("this is an informational message")
l.warning("this is a warning message")
l.error("this is an error message")
l.critical("this is an critical message")