#!/usr/bin/python
# function closures
# any variable availbe during the creation of function,will exist wehn you return 
# the function.

def upper():
	a=10
	def lower():
		return a
	return lower

'''
# foo
	def lower():
		return a
'''

foo = upper()
print foo,type(foo)
print foo() # 10
