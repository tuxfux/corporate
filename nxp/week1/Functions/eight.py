#!/usr/bin/python
# http://simeonfranklin.com/blog/2012/jul/1/python-decorators-in-12-steps/ - TODO

def my_upper(my_func,*args,**kwargs):
	def my_lower(*args,**kwargs):
		try:
			my_func(*args,**kwargs)
		except Exception as e:
			return "Hey i hit on an exception - {}".format(e)
		else:
			return my_func(*args,**kwargs)
	return my_lower

# age old implementation - if you are lucky.
def my_add(a,b):
	return a + b

my_add = my_upper(my_add)


print my_add(11,22)
print my_add(11,"linux")
# print my_div(20,2)
# print my_div(20,0)
# print my_multi('a','b')

# final outcome
# @my_upper
# def my_add(a,b):
# 	return a + b

# @my_upper
# def my_div(a,b):
# 	return a/b

# @my_upper
# def my_multi(a,b):
# 	return a * b

# print my_add(11,22)
# print my_add(11,"linux")
# print my_div(20,2)
# print my_div(20,0)
# print my_multi('a','b')

# caseI
# def my_add(a,b):
# 	try:
# 		a + b
# 	except Exception as e:
# 		return "Hey i hit on an exception - {}".format(e)
# 	else:
# 		return a + b

# def my_div(a,b):
# 	try:
# 		a/b
# 	except Exception as e:
# 		return "Hey i hit on an exception - {}".format(e)
# 	else:
# 		return a/b

# main
# print my_add(11,22)
# print my_add(11,"linux")
# print my_div(20,2)
# print my_div(20,0)