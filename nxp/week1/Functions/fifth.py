#!/usr/bin/python
# tasks: arguments are passed as objects ( google me)

# functional arguments - positional,keybased

def my_add(a,b):
	''' This is for addition of two number a,b '''
	print locals()
	return a + b

# postion based
print my_add(11,12)
print my_add("linux"," rocks")
print my_add(" rocks","linux")

# keybased
print my_add(b=" rocks",a="linux")
print my_add(b="rocks",a=" linux")

# default

# default is not a keywords it a variable
def my_multi(num,default=10):
	for value in range(1,default+1):
		print "{0:2d}*{1:2d} = {2:3d}".format(num,value,num*value)

print my_multi(2)
print my_multi(2,5)
# ex: def putty(host,port=22)
# putty(www.google.com)
# putty(www.google.com,23)

# functional arguments - *,**,*args,**kwargs

# *

def my_add(a,b):
	return a + b

my_list = [11,22]
my_list1 = [11,22,33]
print my_add(*my_list) # i am unpacking my list
print my_add(*my_list1[0:2])


# **

def my_add(a,b):
	return a + b

my_dict = {'a':11,'b':22}
print my_add(**my_dict)
print my_add(*my_dict)

# *args

'''
In [19]: max?
Type:        builtin_function_or_method
String form: <built-in function max>
Namespace:   Python builtin
Docstring:
max(iterable[, key=func]) -> value
max(a, b, c, ...[, key=func]) -> value

With a single iterable argument, return its largest item.
With two or more arguments, return the largest argument.

In [20]: max(2,10)
Out[20]: 10

In [21]: max(-1,-2,-3,-4)
Out[21]: -1

In [22]: max(-1,-2,-3,-4,33,44,91)
Out[22]: 91
'''

def gmax(*args):
	return args # tuple

def gmax(*args):
	big=-1
	for value in args:
		if value > big:
			big = value
	return big

print gmax(2,10)
print gmax(-1,-2,-3,-4)
print gmax(-1,-2,-3,-4,33,44,91)

# **kwargs

def callme(**kwargs):
	for key in kwargs:
		if 'name' in kwargs:
			print "my name is {}".format(kwargs['name'])
		if 'gender' in kwargs:
			print "my gender is {}".format(kwargs['gender'])
		if 'maiden' in kwargs:
			print "my maiden is {}".format(kwargs['maiden'])
		if 'location' in kwargs:
			print "my location is {}".format(kwargs['location']) 

print callme(name='kumar',gender='m')
print callme(name='kumar',maiden='vijaya')
print callme(name='kumar',location='hyd',maiden='vijaya')