#!/usr/bin/python
# namespace or variables(local/global)
# locals() is a inbuild function.
# life span of a variable is during the run time of the function.


def my_func():
	a = 1  # local variable
	print locals()
	return a

## main
print my_func() # 1,none,undefined
print a # not defined