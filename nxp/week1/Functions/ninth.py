#!/usr/bin/python
# map,filter and lamdba




'''
In [35]: filter?
Type:        builtin_function_or_method
String form: <built-in function filter>
Namespace:   Python builtin
Docstring:
filter(function or None, sequence) -> list, tuple, or string

Return those items of sequence for which function(item) is true.  If
function is None, return the items that are true.  If sequence is a tuple
or string, return the same type, else return a list.



In [31]: map?
Type:        builtin_function_or_method
String form: <built-in function map>
Namespace:   Python builtin
Docstring:
map(function, sequence[, sequence, ...]) -> list

Return a list of the results of applying the function to the items of
the argument sequence(s).  If more than one sequence is given, the
function is called with an argument list consisting of the corresponding
item of each sequence, substituting None for missing values when not all
sequences have the same length.  If the function is None, return a list of
the items of the sequence (or a list of tuples if more than one sequence).

In [32]: 

'''
def my_square(a):
	return a * a

def my_even(a):
	if a % 2 == 0:
		return 'even'


print map(my_square,[11,44,22,78,99,91,76])
print filter(my_square,[11,44,22,78,99,91,76])
print filter(my_even,range(11,21))
print map(my_even,range(11,21))

# lambda - nameless function
print map(lambda a:a*a,[11,44,22,78,99,91,76])
print filter(lambda a: a%2 == 0,range(11,21))