#!/usr/bin/python
# if locally we dont have variables we can go globally.
# Local namespace is given higher priority then global scope.

a = 10


# print my_func() # 10,nodefined

def my_func():
	a = 2
	print locals()
	return a

	# case I
def my_func1():
	print locals()
	return a


print my_func() # 
print my_func1()