#!/usr/bin/python
# similar exercise: https://www.datacamp.com/community/tutorials/python-xml-elementtree
import xml.etree.ElementTree as ET

# Open XML document using Element tree parser
Etree = ET.parse('movies.xml')
collection = Etree.getroot()
print collection,type(collection),dir(collection)

print "Root element : {}".format(collection.tag)
print collection.attrib # dictionary

if 'shelf' in collection.attrib:
	print "Root Element Attibute : {}".format(collection.attrib['shelf'])

# Get all the movies in the collection

for movie in collection:
	#print movie,type(movie),movie.attrib
	print "*** Movie ***"
	print "Title: {}".format(movie.attrib['title'])
	#print movie.tag,movie.attrib

	for child in movie:
		print child,type(child),dir(child)
		if child.tag == 'type':
			print "Type: {}".format(child.text)
		if child.tag == 'format':
			print "Format: {}".format(child.text)
		if child.tag == 'rating':
			print "Rating: {}".format(child.text)
		if child.tag == 'stars':
			print "Stars: {}".format(child.text)
		if child.tag == 'description':
			print "Description: {}".format(child.text)


