#!/usr/bin/python
# class - blueprint - account 
# instance - object - rajendra,amrutha

# class Account(object)
class Account:
	balance = 0 					# class variable/data.
	def my_balance(self):
		return "my balance is {}".format(self.balance)

	@classmethod
	def new_balance(cls):		# class method/function
		cls.balance=1000
		return "my balance is - {}".format(cls.balance)

# rajendra - instance
rajendra = Account()
print rajendra,type(rajendra),dir(rajendra)
#print rajendra.balance 
rajendra.balance = 5000
print rajendra.my_balance()
print rajendra.new_balance()

# class - 
print Account.balance
print Account.new_balance()
print Account.balance

print rajendra.my_balance()
# we are able to access the my_balance method as instance.
# we are not able to access the my_balance method via class.
