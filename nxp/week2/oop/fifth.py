# class Account(object)
class Account:
	balance = 0 					# class variable/data.
	def my_balance(self):		# class method/function
		return "my balance is - {}".format(self.balance)

# rajendra
rajendra = Account()
print dir(rajendra)
print "rajendra.balance - {}".format(rajendra.balance)
rajendra.balance=1000
print "Account.balance - {}".format(Account.balance)
print "my balance - {}".format(rajendra.my_balance())


# amrutha
amrutha = Account()
print "amrutha.balance - {}".format(amrutha.my_balance())

'''
# error1
Traceback (most recent call last):
  File "fifth.py", line 11, in <module>
    print "my balance - {}".format(rajendra.my_balance())
TypeError: my_balance() takes no arguments (1 given)

# error2
Traceback (most recent call last):
  File "fifth.py", line 11, in <module>
    print "my balance - {}".format(rajendra.my_balance())
  File "fifth.py", line 5, in my_balance
    return "my balance is - {}".format(balance)
NameError: global name 'balance' is not defined


'''