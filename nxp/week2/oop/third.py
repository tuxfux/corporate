#!/usr/bin/python
# class - blueprint - account 
# instance - object - rajendra,amrutha

# class Account(object)
class Account:
	balance = 0 # class variable/data.

print Account
print type(Account)
print dir(Account)
print Account.balance

# rajendra

rajendra = Account() # instance called rajendra
print rajendra
print type(rajendra)
print dir(rajendra)
print rajendra.balance

# amrutha
amrutha = Account()
amrutha.balance=2000 # instance variable now.

# lets modify the class variable.
Account.balance = 100

print rajendra.balance
print amrutha.balance