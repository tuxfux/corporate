#!/usr/bin/python

def account():
	return {'balance':0}

def my_deposit(ma):
	ma['balance'] = ma['balance'] + 1000
	return ma['balance']

def my_withdraw(ma):
	ma['balance'] = ma['balance'] - 200
	return ma['balance']

# rajendra
rajendra = account()
print rajendra
my_deposit(rajendra)
my_deposit(rajendra)
print "balance after deposit for rajendra - {}".format(rajendra['balance'])
my_withdraw(rajendra)
print "balance after withdraw for rajendra - {}".format(rajendra['balance'])

# amrutha
amrutha = account()
print amrutha