#!/usr/bin/python

## class
class InvalidAgeException(Exception): # child class
	def __init__(self,age):
		self.age = age

# a = InvalidAgeException(10)
# print a,dir(a)
# print a.age,a.message,a.args

# normal function
def validate_age(age):
	if age > 18:
		return "Good buddy you have come to age - {}".format(age)
	else:
		raise InvalidAgeException(age)

## main
if __name__ == '__main__':
	age = int(raw_input("please enter your age:"))
	try:
		validate_age(age)
	except Exception as e:
		print "buddy you are yet to come to age - {}".format(e.age)
	else:
		print validate_age(age)