#!/usr/bin/python

balance=0

def my_deposit():
	global balance
	balance = balance + 1000
	return balance

def my_withdraw():
	global balance
	balance = balance - 200
	return balance

# rajendra
print "Initial balance for rajendra - {}".format(balance)
my_deposit()
print "balance after deposit for rajendra - {}".format(balance)
my_withdraw()
print "balance after withdraw for rajendra - {}".format(balance)

# amruta
print "Initial balance for amruta - {}".format(balance)