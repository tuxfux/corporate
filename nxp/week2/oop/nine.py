#!/usr/bin/python
#https://rszalski.github.io/magicmethods/
'''
__str__(self)
    Defines behavior for when str() 
    is called on an instance of your class.
__repr__(self)
    Defines behavior for when repr() 
    is called on an instance of your class. 
    The major difference between str() and repr() 
    is intended audience. repr() is intended to produce output 
    that is mostly machine-readable (in many cases, 
    	it could be valid Python code even), 
whereas str() is intended to be human-readable.

In [48]: a = 1

In [49]: type(a)
Out[49]: int

In [50]: isinstance(a,int)
Out[50]: True


'''

class RationalNumber:
    """
    Rational Numbers with support for arthmetic operations.

        >>> a = RationalNumber(1, 2)
        >>> b = RationalNumber(1, 3)
        >>> a + b
        5/6
    """
    def __init__(self, numerator, denominator=1):
        self.n = numerator
        self.d = denominator

    def __add__(self, other):
        if not isinstance(other, RationalNumber):
            other = RationalNumber(other)

        n = self.n * other.d + self.d * other.n
        d = self.d * other.d
        return RationalNumber(n, d)

    def __str__(self):
        return "%s/%s" % (self.n, self.d)

    __repr__ = __str__


## main
# case I
a = RationalNumber(1, 2) # a.n = 1,a.d=2
b = RationalNumber(1, 3) # b.n = 1,b.d=3
print a + b # a.__add__(b) => a.__add__(other)
# n = a.n * b.d +  a.d * b.n  => 5
# d = a.d * b.d => 6

# case II
a = 1
b = 2
print a + b

# case III
a = RationalNumber(1, 2)
b = 2 # RationalNumber(2,1)
print a + b # b is my  other
