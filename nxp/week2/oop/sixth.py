#!/usr/bin/python
# __init__ -> constructor
# first method which get initialized when you create a instance.

# class Account(object)
class Account:
	def __init__(self):          # contructor
		self.balance = 0 		 # class variable/data.
	def my_deposit(self,amount):
		self.balance = self.balance + amount
		return self.balance
	def my_withdraw(self,amount):
		self.balance = self.balance - amount
		return self.balance
	def my_balance(self):		# class method/function
		return "my balance is - {}".format(self.balance)

# # main
# print Account,dir(Account)

# rajendra
rajendra = Account()
rajendra.my_deposit(amount=5000)
rajendra.my_withdraw(amount=1000)
print rajendra.my_balance()

# amrutha
amrutha = Account()
amrutha.my_deposit(amount=5000)
amrutha.my_withdraw(amount=500)
print amrutha.my_balance()
