#!/usr/bin/python
# encapsulation
# public,private,protected
# By default your class variables or attributes are public

# class Cup:
# 	color=None
# 	content=None
# 	def fill(self,value):
# 		self.content=value
# 	def empty(self):
# 		self.content=None
# 	def whatin(self):
# 		return "class module:{}".format(self.content)

# public
# red = Cup()
# red.content="coffee"
# print red.content
# print red.whatin()

# class Cup:
# 	# def __init__(self):
# 	# 	self.color=None
# 	# 	self._content=None
# 	color=None
# 	_content=None
# 	def fill(self,value):
# 		self._content=value
# 	def empty(self):
# 		self._content=None
# 	def whatin(self):
# 		return "class module:{}".format(self._content)

# #private # _content
# yellow = Cup()
# yellow.fill("coffee")
# #yellow._content = "yellow"
# #print yellow._content
# print yellow.whatin()

## protected => __

class Cup:
	# def __init__(self):
	# 	self.color=None
	# 	self.__content=None
	color=None
	__content=None
	def fill(self,value):
		self.__content=value
	def empty(self):
		self.__content=None
	def whatin(self):
		return "class module:{}".format(self.__content)

# i am accessing the variable __content outside of instance class.
#print Cup.__content
# print Cup.__content
blue = Cup()
print dir(blue)
blue.fill("coffee")
print blue._Cup__content # name mangling
blue.fill("coffee")
print blue.whatin()

# garbage collecter

