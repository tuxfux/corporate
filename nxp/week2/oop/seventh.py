#!/usr/bin/python
# __init__ -> constructor
# first method which get initialized when you create a instance.

# class Account(object)
class Account:
	def __init__(self):          # contructor
		self.balance = 0 		 # class variable/data.
	def my_deposit(self,amount):
		self.balance = self.balance + amount
		return self.balance
	def my_withdraw(self,amount):
		self.balance = self.balance - amount
		return self.balance
	def my_balance(self):		# class method/function
		return "my balance is -> {}".format(self.balance)

class MinBalanceAccount(Account):
	def __init__(self):
		Account.__init__(self)
	def my_withdraw(self,amount):
		if self.balance - amount < 1000:
			print "Time to call your daddy!!!"
		else:
			Account.my_withdraw(self,amount)


# rajendra - salaried
rajendra = Account()
rajendra.my_deposit(5000)
print rajendra.my_balance()
rajendra.my_withdraw(3000) # dusshera sale
rajendra.my_withdraw(4000) # diwali sale
rajendra.my_withdraw(5000) # dec goa
print rajendra.my_balance()

# Amrutha - studying
Amrutha = MinBalanceAccount()
Amrutha.my_deposit(5000)
print Amrutha.my_balance()
Amrutha.my_withdraw(3000) # dusshera sale
print Amrutha.my_balance()
Amrutha.my_withdraw(2000) # diwali sale
print Amrutha.my_balance()
