#!/usr/bin/python
# @property 
# setters
# deleter

class Employee(object):
	def __init__(self,first,last):
		self.first = first
		self.last = last
		#self.email = first + '.' + last + '@email.com'

	@property
	def email(self):
		return "{}.{}@email.com".format(self.first,self.last)

	@property
	def my_details(self):
		return "{}-{}".format(self.first,self.last)

	@my_details.setter
	def my_details(self,name):
		first,last = name.split(" ")
		self.first = first
		self.last  = last
		#return "{}-{}".format(self.first,self.last)

	@my_details.deleter
	def my_details(self):
		print "hey i am deleting !"
		self.first = None
		self.last = None

# main
emp_1 = Employee('santosh','kumar')

emp_1.first = "tuxfux"

#emp_1.my_details = "abhilash kumar"

print emp_1.first
print emp_1.email
print emp_1.my_details
del emp_1.my_details
print emp_1.my_details