#!/usr/bin/python
import MySQLdb
import csv

user = 'test1' # your username
passwd = 'test1' # your password
host = 'localhost' # your host
db = 'test' # database where your table is stored
table = 'student' # table you want to save

con = MySQLdb.connect(user=user, passwd=passwd, host=host, db=db)
cursor = con.cursor()

query = "SELECT * FROM %s;" % table
cursor.execute(query)

with open('outfile','w') as f:
    writer = csv.writer(f)
    for row in cursor.fetchall():
        writer.writerow(row)
