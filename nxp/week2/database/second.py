#!/usr/bin/python
import re

reg = re.compile('(.*)-.*')

import MySQLdb as mdb
conn = mdb.connect('localhost','test1','test1','test')
# conn = mdb.connect(server,user,pass,database).
cur = conn.cursor()
cur.execute("create table student(name varchar(10),gender varchar(6))")
conn.close()

'''
mysql> show tables;
Empty set (0.00 sec)

mysql> show tables;
+----------------+
| Tables_in_test |
+----------------+
| student        |
+----------------+
1 row in set (0.00 sec)

mysql> desc student;
+--------+-------------+------+-----+---------+-------+
| Field  | Type        | Null | Key | Default | Extra |
+--------+-------------+------+-----+---------+-------+
| name   | varchar(10) | YES  |     | NULL    |       |
| gender | varchar(6)  | YES  |     | NULL    |       |
+--------+-------------+------+-----+---------+-------+
2 rows in set (0.01 sec)

mysql> 
'''