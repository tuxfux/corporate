from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Note(models.Model):
	name = models.CharField(max_length=10)
	gender = models.CharField(max_length=6)
	
	def __str__(self):
		return '%s %s' % (self.name, self.gender)
