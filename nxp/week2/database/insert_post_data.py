#!/usr/bin/python
# create

import requests
import json

name = raw_input("please enter your name:")
gender = raw_input("please enter your gender")

post_url = 'http://127.0.0.1:8000/api/note/'
post_data = {'name': name, 'gender':gender}
headers = {'Content-type': 'application/json'}
r = requests.post(post_url,json.dumps(post_data),headers=headers)
print r.status_code

# note 201 status is the status for post request
if r.status_code != 201: 
    print('Status:', r.status_code, 'Headers:', r.headers, 'Error Response:',r.json())
    exit()
