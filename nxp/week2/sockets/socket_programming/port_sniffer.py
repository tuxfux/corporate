#!/usr/bin/python

import socket,sys

#print sys.argv,len(sys.argv)

if len(sys.argv) != 3:
	print """
		./port_sniffer.py <hostname> <portname>
	"""
	sys.exit()
else:
	host = sys.argv[1]
	port = int(sys.argv[2])

try:
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
except socket.error,error:
    print "Failed to create a socket . Error code - {} , Error message - {}".format(str(error[0]),str(error[1]))
    sys.exit()

# nslookup on top of the host.

try:
	remote_ip = socket.gethostbyname(host)
except:
	print "Hostname could not be resolved."
	sys.exit()


# connect to a host/server

s.connect((host,port))

print "IP:{} and Host:{} port:{}".format(remote_ip,host,port)

