#!/usr/bin/python

import socket,sys
from thread import start_new_thread

# variables
host = 'localhost' # 127.0.0.1
port = 8888

# create a socket

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

# bind out ip to the port

try:
	s.bind((host,port))
except socket.error,msg:
	print "Bind Failed: Error Code :{} , message: {}".format(str(msg[0]),msg[1])
	sys.exit()

## listening
s.listen(10)
print "Socket now listening"

# target is a simple function.
def clientthread(conn):
	conn.send("Welcome to the server.Try to type something and hit enter \n")

	while True:
		data = conn.recv(1024) # receiving data from client
		reply = "OK ..." + data
		if not data:
			break
		conn.sendall(reply)
	conn.close()


# accepting the requests
while True:
	conn,addr = s.accept()
	print "connected with:{},addr:{}".format(addr[0],str(addr[1]))
	start_new_thread(clientthread,(conn,))

s.close()