#!/usr/bin/python

import socket,sys

try:
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
except socket.error,error:
    print "Failed to create a socket . Error code - {} , Error message - {}".format(str(error[0]),str(error[1]))
    sys.exit()

host = "www.google.com"

# nslookup on top of the host.

try:
	remote_ip = socket.gethostbyname(host)
except:
	print "Hostname could not be resolved."
	sys.exit()


# connect to a host/server
port = 80 # http
s.connect((host,port))

print "IP:{} and Host:{} port:{}".format(remote_ip,host,port)

## send some data outside

#message = raw_input("enter the message you want to pass to:")
message = "GET / HTTP /1.1\r\n\r\n"

try:
	s.sendall(message)
except socket.error:
	print "Send failed"
	sys.exit()

print "Message sending successfully"

## receive the data from system

reply = s.recv(4096)
print reply



