#!/usr/bin/python
# virtual memory
import multiprocessing as m

final_output = []

def my_square(num):
    global final_ouput
    for value in num:
        print "square:{}".format(value**2)
        final_output.append(value**2)
    print final_output

# main

if __name__ == '__main__':
    num = [11,22,33,44,55,66]
    p1 = m.Process(target=my_square,args=(num,))
    p1.start()
    p1.join()

print final_output