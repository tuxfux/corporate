#!/usr/bin/python
import time
import threading as t

def my_square(num):
	for value in num:
		time.sleep(5)
		print "square:{}".format(value**2)

def my_cube(num):
	for value in num:
		time.sleep(5)
		print "cube:{}".format(value**3)

if __name__ == '__main__':
	num = [11,22,33,44,55,66]
	T=time.time()
	my_square(num)
	# t1 = t.Thread(target=my_square,args=(num,))
	# t2 = t.Thread(target=my_cube,args=(num,))
	# t1.start()
	# t2.start()
	# t1.join()
	# t2.join()
	my_cube(num)
	print "time taken - {}".format(time.time() - T)



