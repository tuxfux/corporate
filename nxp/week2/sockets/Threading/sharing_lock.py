#!/usr/bin/python
# multiprocessing lock

import time
import multiprocessing as m

def deposit(balance):
  for i in range(100):
    time.sleep(0.01)
    lock.acquire()
    balance.value = balance.value + 1
    lock.release()

def withdraw(balance):
  for i in range(100):
    time.sleep(0.01)
    lock.acquire()
    balance.value = balance.value - 1
    lock.release()

if __name__ == '__main__':
  balance = m.Value('i',200) # balance variable
  lock = multiprocessing.Lock() # setting a lock
  d = m.Process(target=deposit,args=(balance,lock))
  w = m.Process(target=withdraw,args=(balance,lock))
  d.start()
  w.start()
  d.join()
  w.join()

print "my balance - {}".format(balance.value)
