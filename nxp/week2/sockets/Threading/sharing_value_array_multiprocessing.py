#!/usr/bin/python
# shared memory variable - arrays and variables

import multiprocessing as m

def my_square(num,array_num,value_num):
	value_num.value = 3.142
	print num
	for x,y in enumerate(num):
		print "x:{}-y:{}".format(x,y)
		array_num[x] = y*y 
		
# main

if __name__ == '__main__':
	num = [11,22,33,44,55,66]
	array_num = m.Array('i',6) # array value
	print type(array_num),type(array_num)
  	value_num = m.Value('d',3.456) # variable value
	p1 = m.Process(target=my_square,args=(num,array_num,value_num))
	p1.start()
	p1.join()

print array_num[:]
print value_num.value
