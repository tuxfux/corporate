#!/usr/bin/python
# numa
# not all cores working as expected.
# map -> divide work between multiple cores.
# reduce -> aggregate from multiple cores to a single output.
import time
from multiprocessing import Pool

def square(n):
  return n * n

# main
if __name__ == '__main__':
  value = [1,2,3,4,5]
  P = Pool()
  result = P.map(square,value)
  print result

