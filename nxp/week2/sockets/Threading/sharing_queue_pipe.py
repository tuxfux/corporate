#!/usr/bin/python
# virtual memory
# multiprocessing queue and piping 

import multiprocessing as m


def my_square(num,q1):
	for value in num:
		q1.put(value*value)

# main

if __name__ == '__main__':
	num = [11,22,33,44,55,66]
	q1 = m.Queue()  # queue method
	p1 = m.Process(target=my_square,args=(num,q1))
	p1.start()
	p1.join()

while q1.empty() is False:
	print(q1.get())
