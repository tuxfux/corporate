#!/usr/bin/python
# numa
# not all cores working as expected.
# map -> divide work between multiple cores.
# reduce -> aggregate from multiple cores to a single output.
import time
import multiprocessing as m

def square(n):
  return n * n

# main
if __name__ == '__main__':
  value = [1,2,3,4,5]

  result = []
  for n in value:
    result.append(square(n))

print result
  

