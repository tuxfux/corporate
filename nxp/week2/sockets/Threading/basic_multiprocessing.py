#!/usr/bin/python
#https://techdifferences.com/difference-between-multiprocessing-and-multithreading.html
#https://searchstorage.techtarget.com/definition/virtual-memory
import time
import multiprocessing as m

def my_square(num):
	for value in num:
		time.sleep(2)
		print "square:{}".format(value**2)

def my_cube(num):
	for value in num:
		time.sleep(2)
		print "cube:{}".format(value**3)

if __name__ == '__main__':
	num = [11,22,33,44,55,66]
	T=time.time()
	#my_square(num)
	t1 = m.Process(target=my_square,args=(num,))
	print t1.name
	t2 = m.Process(target=my_cube,args=(num,))
	t1.start()
	print t1.pid,t1.ident
	t2.start()
	print t2.pid,t2.ident
	t1.join()
	t2.join()
	#my_cube(num)
	print "time taken - {}".format(time.time() - T)



